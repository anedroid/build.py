build.py

# Configs

* **\[defaults\]** – default frontmatter fields values (i.e. `author=faildev_mode`)
* **\[variables\]** – project-wide variables (i.e. `root=rawtext.club/~faildev_mode`)
* **\[variables.gemini\]** – project-wide variables gemini-exclusive
* **\[variables.www\]** – project-wide variables www-exclusive
* **\[redirections\]** – redirection table (i.e. `youtube.com=invidious.citizen4.eu`)
* **\[templates\]** – template routing table (i.e. `articles/*.gmi=articles`)

# Special frontmatter fields

* **mode** (gemini, www) – mark this file as gemini/www-exclusive. If not specified, will be parsed for both

# Parsing order:

1. Content: gemini
2. Template: gemini
3. Content: www
4. Template: www

```python
# simplified for clarity
for path in content_files:
	item = Item(path)
	if item.mode == 'gemini' or not item.mode:
		content = evaluate(item.content, {mode='gemini'})
		content = evaluate(item.template, {mode='gemini', item=item})
		save_this(f'{item.location}.gmi', content)
	
	if item.mode == 'www' or not item.mode:
		content = evaluate(item.content, {mode='www'})
		content = evaluate(item.template, {mode='www', item=item})
		content = gemtext2html(content)
		content = evaluate(item.html_template, {item=item})
		save_this(f'html/{item.location}.html', content)
```

# Modes and templates analysis

Build.py mode flexibility is actually limited to just content files, i.e. images cannot have frontmatter or evaluations. Static files are designed for providing static binary files like images, pdfs, etc.

However content files does not need to be gemtext files, but also xml files. Using evaluations and `mode` variable you can know whether you are in gemini or www and i.e. adapt links in your RSS feed. In most cases this is the only place you would ever need to use this feature. This might also be used for web discrimination through „nasty” templates – if you open the „premium” article on www, you can only read several paragraphs and then meet the „gemwall”. Using some frontmatter fields you could also mark some articles as „premium” to have icon next to the link at menu.

Sometimes you might want to use html templates instead of gemtext ones, because it looks better.
```ini
[templates]
articles/*.gmi = article.gmi
*.html = www.html
articles/*.html = article.html
```
With the example config above, after conversion to html `www.html` template will be applied, except for articles, which will use `article.html`, because the last matching glob will be selected. Normally both `article.gmi` and `article.html` will influence the final html file. Replacing the gemini template in www mode is possible by adding `mode=gemini` to `templates/article.gmi` frontmatter. Setting `mode=www` on templates probably never will be useful.

Then what about html content files, that is, content files being html as the source, not the product? What about other non-static files like xml? Build.py follows these rules:
* apply templates whenever possible,
* convert gemtext to html if  `mode=www` or no mode,
* keep gemini version if `mode=gemini` or no mode,
* the html template is applied despite the source file is html or gemtext,
* the variable `mode` is passed to evaluation scripts indicating current parsing mode.

# Globals precedence:

1. path, ~~content~~
2. APIs
3. \*config.variables
4. \*config.variables.gemini, \*config.variables.www
5. ~~frontmatter_data = config.defaults~~
6. ~~frontmatter_data += item.frontmatter_data~~
7. \*item.frontmatter_data
8. mode
