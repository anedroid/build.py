This repo is just a playground, absolutely not production-ready. Don't expect anything to work.

Project notes [here](NOTES.md), documentation and/or proper README comming soon.

## Dependencies

* python-frontmatter
* beautifulsoup4
