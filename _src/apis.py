import os
from item import Item # from itself
import datetime

class Content:
    """This class contains some functions useful for evaluation of Python code
    in content files
    """
    
    __builtins__ = {
        'sorted': sorted
    }
    
    def log(dest: str) -> str:
        """Returns path to specified log"""
        
        ext = '.gmi' if environment['mode'] == 'gemini' else '.html'
        return '/gemlog/'+dest+ext
    
    def article(dest: str) -> str:
        """Returns path to specified article"""
        
        ext = '.gmi' if environment['mode'] == 'gemini' else '.html'
        return '/articles/'+dest+ext
    
    def res(dest: str, label=None) -> str:
        """Returns path to specified resource (usually image), which resides in
        a directory of the name of current file
        """
        
        path = os.path.splitext(environment['path'])[0]
        path += '/'+dest
        
        if not os.path.exists('static/'+path):
            raise Exception(f'Resource {dest} for {path} not found')
        
        if label:
            return '/'+path+' '+label
        else:
            return '/'+path
    
    def dir(dir: str):
        """Iterator of Item objects representing files in specified directory
        (like os.scandir)
        """
        
        for entry in os.scandir('content/'+dir):
            item = Item(os.path.relpath(entry.path, start='content'))
            if item.title: yield item
    
    def date(src) -> str:
        """Returns date string from datetime or date object"""
        
        if type(src) != datetime.time:
            return src.strftime('%Y-%m-%d')
        return ''
    
    def time(src) -> str:
        """Returns time string from datetime or time object"""
        
        if type(src) != datetime.date:
            return src.strftime('%H:%M')
        return ''
