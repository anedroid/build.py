import configparser
"""This module is meant to store global config across the entire application.

from config import config
config.read('config.ini')
"""

config = configparser.ConfigParser(interpolation=configparser.ExtendedInterpolation())
