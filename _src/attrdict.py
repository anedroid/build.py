class AttrDict(dict):
    """This dict-based class allows accessing keys like attributes
    https://stackoverflow.com/questions/4984647/accessing-dict-keys-like-an-attribute
    """
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__dict__ = self
