-i https://pypi.org/simple
beautifulsoup4==4.12.2
python-frontmatter==1.0.0
pyyaml==6.0; python_version >= '3.6'
soupsieve==2.4.1; python_version >= '3.7'
